package fr.dawan.formationjpa.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.onetoone.Maire;
import fr.dawan.formationjpa.entities.onetoone.Ville;

public class TestOneToOne4 {

    public static void main(String[] args) {
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        EntityTransaction tx=em.getTransaction();
        
        Ville lille=new Ville("Lille");
        Maire martine=new Maire("Martine","Aubry");
        martine.setVille(lille);
        System.out.println(lille.getMaire());
        lille.setMaire(martine);
        try {
            tx.begin();
            em.persist(lille);
            em.persist(martine);

            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        System.out.println(martine);
        
        em.close();
        emf.close();
    }

}
