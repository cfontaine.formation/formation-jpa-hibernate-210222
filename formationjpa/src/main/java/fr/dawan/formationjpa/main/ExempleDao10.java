package fr.dawan.formationjpa.main;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.dao.ArticleDao;
import fr.dawan.formationjpa.entities.many.Article;

public class ExempleDao10 {

    public static void main(String[] args) {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        ArticleDao arDao=new ArticleDao(em);
        Article a1=new Article("Stylo bille noir", 1.5,LocalDate.of(2018, 5, 24));
        try {
            arDao.saveOrUpdate(a1);
            List<Article> lstAr=arDao.findAll();
            lstAr.forEach(System.out::println);
            a1.setDateAjout(LocalDate.of(2017, 3, 7));
            arDao.saveOrUpdate(a1);
            Article a2=arDao.findById(a1.getId());
            System.out.println(a2);
            arDao.remove(a1);
            arDao.findAll().forEach(System.out::println);
            lstAr=arDao.findByPrixInf(150.0);
            lstAr.forEach(System.out::println);            
        } catch (Exception e) {
            e.printStackTrace();
        }
        

    }

}
