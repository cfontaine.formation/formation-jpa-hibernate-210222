package fr.dawan.formationjpa.main;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.Adresse;
import fr.dawan.formationjpa.entities.Personne;
import fr.dawan.formationjpa.entities.bibliotheque.Nation;
import fr.dawan.formationjpa.enums.Civilite;

public class TestJPA1 {

    public static void main(String[] args) {
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        EntityTransaction tx=em.getTransaction();
        
        Personne p1=new Personne("John", "Doe", "jd@dawan.com");
       // p1.setId(1);
        p1.setCivilite(Civilite.MONSIEUR);
        p1.setDateNaissance(LocalDate.of(1995,3,4));
        p1.setAdressePerso(new Adresse("rue solferino","Lille","59000"));
        p1.setAdressePro(new Adresse("46, rue des cannoniers","Lille","59000"));
        
        Nation n1=new Nation("Espagne");
        Nation n2=new Nation("Belgique");
        Personne p2=null;
        try {
            tx.begin();
            em.persist(n1);
            em.persist(p1);
            p1.setDateNaissance(LocalDate.of(1998,7,4));
            //  em.remove(p1);
            
//            p2=em.find(Personne.class, 1L);
//            em.lock(p2,  LockModeType.PESSIMISTIC_WRITE);
            
//            p2=em.find(Personne.class, 1L,LockModeType.PESSIMISTIC_WRITE);
//            p2.setEmail("jdoe.dawan.com");
            //
            em.persist(n2);
            
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        System.out.println(p2);
        em.close();
        System.out.println(p2);
        emf.close();
       // System.out.println(p2);
    }

}
