package fr.dawan.formationjpa.main;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.many.Article;
import fr.dawan.formationjpa.entities.many.Fournisseur;
import fr.dawan.formationjpa.entities.many.Marque;

public class TestManyToOne5 {

    public static void main(String[] args) {
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        EntityTransaction tx=em.getTransaction();
        
        Article ar1=new Article("TV 4K",600.0,LocalDate.of(2021, 11, 3));
        Article ar2=new Article("SmartPhone Android",350.0,LocalDate.of(2021, 7, 23));
        Article ar3=new Article("Souris Gaming",30.0,LocalDate.of(2020, 1, 16));
        Marque ma=new Marque("Marque A");
        Marque mb=new Marque("Marque B");
        ar1.setMarque(ma);
        
        ar2.setMarque(ma);
        ma.getArticles().add(ar1);
        ma.getArticles().add(ar2);
        ar3.setMarque(mb);
        mb.getArticles().add(ar3);
        
        Fournisseur f1=new Fournisseur("Fournisseur1");
        Fournisseur f2=new Fournisseur("Fournisseur2");
   //     ar1.getFournisseurs().add(f1);
        f1.getArticles().add(ar1);
    //    ar2.getFournisseurs().add(f1);
        f1.getArticles().add(ar2);
    //    ar2.getFournisseurs().add(f2);
        f2.getArticles().add(ar2);
   //     ar3.getFournisseurs().add(f2);
        f2.getArticles().add(ar3);
    
        try {
            tx.begin();
            em.persist(ma);
            em.persist(mb);
            em.persist(ar1);
            em.persist(ar2);
            em.persist(ar3);
            em.persist(f1);
            em.persist(f2);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }

        
        em.close();
        emf.close();

    }

}
