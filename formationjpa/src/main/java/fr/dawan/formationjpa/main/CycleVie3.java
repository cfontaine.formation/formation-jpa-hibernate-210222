package fr.dawan.formationjpa.main;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.Adresse;
import fr.dawan.formationjpa.entities.Personne;
import fr.dawan.formationjpa.enums.Civilite;

public class CycleVie3 {

    public static void main(String[] args) {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        EntityTransaction tx= em.getTransaction();
        
        Personne p1=new Personne("John", "Doe", "jd@dawan.com");
         p1.setCivilite(Civilite.MONSIEUR);
         p1.setDateNaissance(LocalDate.of(1995,3,4));
         p1.setAdressePerso(new Adresse("rue solferino","Lille","59000"));
         p1.setAdressePro(new Adresse("46, rue des cannoniers","Lille","59000"));
         
         System.out.println(p1);
//         
//         try {
//            tx.begin();
//             em.persist(p1);
//             System.out.println(p1);
//             //em.detach(p1);
//            // em.clear();
//             p1.setEmail("jd@jehann.com");
////            p1= em.merge(p1);
////             em.remove(p1);
//             tx.commit();
//        } catch (Exception e) {
//            tx.rollback();
//            e.printStackTrace();
//        }
//         System.out.println(p1);
//        
//        em.close();
//        EntityManager em2=emf.createEntityManager();
//        tx=em2.getTransaction();
//        try {
//            tx.begin();
//            em2.remove(em2.merge(p1));
//            tx.commit();
//        } catch (Exception e) {
//            tx.rollback();
//            e.printStackTrace();
//        }
//        em2.close();
        
        //find
//        try {
//            tx.begin();
//            Personne p2=em.find(Personne.class, 2L);
//            System.out.println(p2);
//            em.remove(p2);
//            tx.commit();
//        }
//        catch(Exception e) {
//            tx.rollback();
//        }
        
        //refresh
        try {
            tx.begin();
            em.persist(p1);
            tx.commit();
            System.out.println("_____________________");
            Thread.sleep(30000);
            System.out.println("_____________________");
            System.out.println(p1);
            tx.begin();
            em.refresh(p1);
            tx.commit();
            System.out.println(p1);
        }
        catch(Exception e) {
            tx.rollback();
        }
        emf.close();

    }

}
