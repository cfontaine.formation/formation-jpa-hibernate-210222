package fr.dawan.formationjpa.main;

import java.io.IOException;
import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.films.Film;
import fr.dawan.formationjpa.entities.films.FilmSpectateur;
import fr.dawan.formationjpa.entities.films.Spectateur;

public class ExempleDonneeTableJointure8 {

    public static void main(String[] args) {
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        EntityTransaction tx=em.getTransaction();
        
        Film akira=new Film("Akira",LocalDate.of(1988, 7, 16));
        try {
            akira.setAffiche(Film.loadImage("AKIRA.jpg"));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        Film alien=new Film("Alien",LocalDate.of(1979,1,1));
        Film aliens=new Film("Aliens",LocalDate.of(1986, 1, 1));
        
        Spectateur sp1=new Spectateur("John", "Doe");
        Spectateur sp2=new Spectateur("Alan", "Smithee");
        
        FilmSpectateur fs=new FilmSpectateur(akira, sp1, 9);
        FilmSpectateur fs1=new FilmSpectateur(alien, sp1, 7);
        FilmSpectateur fs2=new FilmSpectateur(akira, sp2, 8);
        FilmSpectateur fs3=new FilmSpectateur(aliens, sp2, 8);
        FilmSpectateur fs4=new FilmSpectateur(aliens, sp1, 5);
        
        akira.getNotes().add(fs);
        akira.getNotes().add(fs2);
        alien.getNotes().add(fs1);
        aliens.getNotes().add(fs3);
        aliens.getNotes().add(fs4);
        
        sp1.getNotes().add(fs);
        sp1.getNotes().add(fs1);
        sp1.getNotes().add(fs4);
        sp2.getNotes().add(fs2);
        sp2.getNotes().add(fs3);
        
        try {
            tx.begin();
            em.persist(akira);
            em.persist(alien);
            em.persist(aliens);
            em.persist(sp1);
            em.persist(sp2);
            System.out.println(akira);
            em.persist(fs);
            em.persist(fs1);
            em.persist(fs2);
            em.persist(fs3);
            em.persist(fs4);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.close();
        emf.close();

    }

}
