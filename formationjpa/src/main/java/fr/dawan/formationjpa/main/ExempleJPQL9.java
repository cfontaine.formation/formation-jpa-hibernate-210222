package fr.dawan.formationjpa.main;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import fr.dawan.formationjpa.entities.many.Article;
import fr.dawan.formationjpa.entities.many.ArticleSimple;
import fr.dawan.formationjpa.entities.many.Fournisseur;
import fr.dawan.formationjpa.entities.many.Marque;

public class ExempleJPQL9 {

    public static void main(String[] args) {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
       // EntityTransaction tx= em.getTransaction();
        
        // SQL SELECT * FROM articles
        TypedQuery<Article> query=em.createQuery("SELECT a FROM Article AS a", Article.class);
        List<Article> articles=query.getResultList();
        for(Article a : articles) {
            System.out.println(a + " " + a.getMarque());
        }
        
        Query q1=em.createQuery("SELECT a.prix, a.description FROM Article a");
        List<Object[]> lstObj=q1.getResultList();
        for(Object[] tObj : lstObj) {
            System.out.println((Double)tObj[0] + " " + (String) tObj[1]);
        }
        
        
        TypedQuery<ArticleSimple> qr1=em.createQuery("SELECT new fr.dawan.formationjpa.entities.many.ArticleSimple(a.prix,a.description) FROM Article a", ArticleSimple.class);
        List<ArticleSimple> lstArtSimple=qr1.getResultList();
        lstArtSimple.forEach(System.out::println);
        
        TypedQuery<Article> qr2=em.createQuery("SELECT a FROM Article a WHERE a.prix<50.0", Article.class);
        List<Article> lstArtinf50=qr2.getResultList();
        lstArtinf50.forEach(System.out::println);
        
        TypedQuery<Article> qr3=em.createQuery("SELECT a FROM Article a WHERE NOT a.prix<50.0", Article.class);
        List<Article> lstArtsSupEqu50=qr3.getResultList();
        lstArtsSupEqu50.forEach(System.out::println);
        
        TypedQuery<Article> qr4=em.createQuery("SELECT a FROM Article a WHERE  a.prix<30.0 OR a.prix>200.0", Article.class);
        List<Article> lstArtsInf30Sup200=qr4.getResultList();
        lstArtsInf30Sup200.forEach(System.out::println);
        
        // Litéral
        TypedQuery<Article> qr5=em.createQuery("SELECT a FROM Article a WHERE  a.description='Tv 4k'", Article.class);
        List<Article> lstArtsTV4K=qr5.getResultList();
        lstArtsTV4K.forEach(System.out::println); 
        
        TypedQuery<Article> qr6=em.createQuery("SELECT a FROM Article a WHERE  a.dateAjout<'2021-11-01'", Article.class);
        List<Article> lstArtinf202111=qr6.getResultList();
        lstArtinf202111.forEach(System.out::println); 
        
        Query qr7=em.createQuery("SELECT a.prix*0.25 FROM Article a WHERE  a.dateAjout<'2021-11-01'");
        List<Double> lstArtTaxe=qr7.getResultList();
        for(double val : lstArtTaxe) {
            System.out.println(val);
        } 
        
        // Paramètres
        TypedQuery<Article> qr8=em.createQuery("SELECT a FROM Article a WHERE a.prix< :prix",Article.class);
        qr8.setParameter("prix", 50.0);
        List<Article> lstArtInfParam=qr8.getResultList();
        lstArtInfParam.forEach(System.out::println);
        
        qr8=em.createQuery("SELECT a FROM Article a WHERE a.prix< ?1",Article.class);
        qr8.setParameter(1, 50.0);
        lstArtInfParam=qr8.getResultList();
        lstArtInfParam.forEach(System.out::println);
        
        // Where
        TypedQuery<Article> qr9=em.createQuery("SELECT a FROM Article a WHERE a.dateAjout BETWEEN '2021-10-01' AND '2022-01-01'",Article.class);
        qr9.getResultList().forEach(System.out::println);
        
        qr9=em.createQuery("SELECT a FROM Article a WHERE a.prix IN (20.0,40.0,80.0)",Article.class);
        qr9.getResultList().forEach(System.out::println);
        
        qr9=em.createQuery("SELECT a FROM Article a WHERE a.marque IS NULL",Article.class);
        qr9.getResultList().forEach(System.out::println);
        
        qr9=em.createQuery("SELECT a FROM Article a WHERE a.description LIKE 'T__%'",Article.class);
        qr9.getResultList().forEach(System.out::println);
        
        qr9=em.createQuery("SELECT a FROM Article a WHERE a.description LIKE '%@%' ESCAPE '@'",Article.class);
        qr9.getResultList().forEach(System.out::println);
        
        qr9=em.createQuery("SELECT a FROM Article a WHERE a.description LIKE :nom",Article.class); 
        qr9.setParameter("nom","%TV%");
        qr9.getResultList().forEach(System.out::println);
        
        // Collection
        TypedQuery<Marque> qr10=em.createQuery("SELECT m FROM Marque m WHERE m.articles IS EMPTY",Marque.class);
        qr10.getResultList().forEach(System.out::println);
        
        // Fonctions
        TypedQuery<Long> qr11=em.createQuery("SELECT COUNT(a) FROM Article a",Long.class);
        System.out.println(qr11.getSingleResult());
        qr11=em.createQuery("SELECT COUNT(a) FROM Article a WHERE a.prix<40.0",Long.class);
        System.out.println(qr11.getSingleResult());
        
        // Fonction Chaine de caratcères
        TypedQuery<String> qr12=em.createQuery("SELECT CONCAT('Description: ', a.description,' prix=',a.prix) FROM Article a",String.class);
        qr12.getResultList().forEach(System.out::println);
        
        qr12=em.createQuery("SELECT SUBSTRING(UPPER(a.description),1,3) FROM Article a",String.class);
        qr12.getResultList().forEach(System.out::println);
        
        
        qr12=em.createQuery("SELECT TRIM (TRAILING FROM (CONCAT('      ',a.description,'       '))) FROM Article a",String.class);
        qr12.getResultList().forEach(System.out::println);
        
        TypedQuery<Integer>  qr13=em.createQuery("SELECT LENGTH(a.description) FROM Article a",Integer.class);
        qr13.getResultList().forEach(System.out::println);
        
        qr13=em.createQuery("SELECT LOCATE ('DDR',a.description) FROM Article a",Integer.class);
        qr13.getResultList().forEach(System.out::println);
    
        qr13=em.createQuery("SELECT SIZE(m.articles) FROM Marque m WHERE m.articles IS NOT EMPTY",Integer.class);
        qr13.getResultList().forEach(System.out::println);
        
        TypedQuery<Date> qr14=em.createQuery("SELECT DISTINCT CURRENT_DATE FROM Article a",Date.class);
        System.out.println(qr14.getSingleResult());
        
        
        qr9=em.createQuery("SELECT a FROM Article a WHERE a.prix<100.0 Order BY a.prix DESC, a.description",Article.class); 
        qr9.getResultList().forEach(System.out::println);
        
        qr12=em.createQuery("SELECT CONCAT( a.prix, ' ', Count(a))  FROM Article a GROUP BY a.prix HAVING  Count(a)>1",String.class);
        qr12.getResultList().forEach(System.out::println);
        //SELECT ... FROM articles INNER JOIN marques ON articles.marque_fk=marques.id
        qr9=em.createQuery("SELECT a FROM Article a Join a.marque m WHERE m.nom='Marque A'",Article.class);
        qr9.getResultList().forEach(System.out::println);
        
        qr10=em.createQuery("SELECT DISTINCT m FROM Article a Join a.marque m",Marque.class);
        qr10.getResultList().forEach(System.out::println);
        
        qr12=em.createQuery("SELECT CONCAT(m.nom,' ',Count(a)) FROM Marque m LEFT Join m.articles a GROUP BY m.nom",String.class);
        qr12.getResultList().forEach(System.out::println);
        
        // SOUS REQUETE
        TypedQuery<Fournisseur> qr15= em.createQuery("SELECT f FROM Fournisseur f WHERE (SELECT a FROM Article a WHERE a.prix= 650.0)  MEMBER OF f.articles",Fournisseur.class);
        qr15.getResultList().forEach(System.out::println);
        
        
        // Requete Nommée
        TypedQuery<Article> qr16= em.createNamedQuery("Article.articleInfParam", Article.class);
        qr16.setParameter("prix", 100.0)
        .getResultList().forEach(System.out::println);
        
        qr16= em.createNamedQuery("Article.articleInterParam", Article.class);
        qr16.setParameter("prixMin", 50.0)
        .setParameter("prixMax", 150.0)
        .getResultList().forEach(System.out::println);
        

//        try {
//         // UPDATE
//            em.getTransaction().begin();
//            Query update=em.createQuery("UPDATE Article a SET a.prix=a.prix+1.0 WHERE a.prix<:prixMax");
//            update.setParameter("prixMax", 100.0);
//            System.out.println(update.executeUpdate());
//            em.getTransaction().commit();
//        } catch (Exception e) {
//            em.getTransaction().rollback();
//            e.printStackTrace();
//        }
        
        
        try {
            // DELETE
               em.getTransaction().begin();
               Query update=em.createQuery("DELETE FROM Article a WHERE a.prix=10.0");

               System.out.println(update.executeUpdate());
               em.getTransaction().commit();
           } catch (Exception e) {
               em.getTransaction().rollback();
               e.printStackTrace();
           }
        // JOIN FETCH
//        qr10=em.createQuery("SELECT DISTINCT m FROM Marque m JOIN FETCH m.articles a ",Marque.class);
//        List<Marque> lstMarque=qr10.getResultList();
//        lstMarque.forEach(System.out::println);
        em.close();
//        System.out.println("-------------------------");
        
//        lstMarque.get(1).getArticles().forEach(System.out::println);
        
        
        emf.close();
        
        

    }

}
