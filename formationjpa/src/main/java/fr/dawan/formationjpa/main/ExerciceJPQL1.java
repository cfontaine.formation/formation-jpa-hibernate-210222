package fr.dawan.formationjpa.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import fr.dawan.formationjpa.entities.bibliotheque.Auteur;
import fr.dawan.formationjpa.entities.bibliotheque.Livre;

public class ExerciceJPQL1 {

    public static void main(String[] args) {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        
        TypedQuery<Livre> qrLivre=em.createQuery("SELECT l FROM Livre l WHERE l.anneeSortie=:annee ", Livre.class);
        qrLivre.setParameter("annee", 1986)
        .getResultList().forEach(System.out::println);
        
        qrLivre=em.createQuery("SELECT l FROM Livre l WHERE l.anneeSortie BETWEEN :min AND :max ", Livre.class);
        qrLivre.setParameter("min", 1955)
        .setParameter("max", 1964)
        .getResultList().forEach(System.out::println);
        
        qrLivre=em.createQuery("SELECT l FROM Livre l WHERE l.anneeSortie IN (1962,1988,1992) ", Livre.class);
        qrLivre.getResultList().forEach(System.out::println);
        
        qrLivre=em.createQuery("SELECT l FROM Livre l WHERE l.titre LIKE 'D__%'", Livre.class);
        qrLivre.getResultList().forEach(System.out::println);
        
        TypedQuery<Long>qrLong=em.createQuery("SELECT COUNT(l) FROM Livre l WHERE l.anneeSortie = :annee", Long.class);
        long nbLivre=qrLong.setParameter("annee", 1986).getSingleResult();
        System.out.println(nbLivre);
        
        qrLong=em.createQuery("SELECT COUNT(a) FROM Auteur a WHERE a.deces IS NULL",Long.class);
        System.out.println(qrLong.getSingleResult());
        
        TypedQuery<String> qrStr=em.createQuery("SELECT CONCAT(l.anneeSortie,'->',Count(l)) FROM Livre l GROUP BY l.anneeSortie ORDER BY l.anneeSortie DESC", String.class); 
        qrStr.getResultList().forEach(System.out::println);
        
        qrStr=em.createQuery("SELECT CONCAT(a.prenom,' ',a.nom) FROM Auteur a", String.class); 
        qrStr.getResultList().forEach(System.out::println);
        
        qrStr=em.createQuery("SELECT CONCAT(SUBSTRING(a.nom,1,3),'-',a.id,'-',SUBSTRING(a.prenom,LENGTH(a.prenom),1),'-',LENGTH(a.nom) )FROM Auteur a", String.class); 
        qrStr.getResultList().forEach(System.out::println);
        
        qrLong=em.createQuery("SELECT DISTINCT COUNT(a.prenom) FROM Auteur a", Long.class);
        System.out.println(qrLong.getSingleResult());

        qrLivre=em.createQuery("SELECT l FROM Livre l WHERE l.anneeSortie BETWEEN :annee-MOD(:annee,10) AND :annee+9-MOD(:annee,10) ORDER BY l.anneeSortie", Livre.class);
        qrLivre.setParameter("annee", 1988).getResultList().forEach(System.out::println);
        
        em.close();
        emf.close();
    }

}
