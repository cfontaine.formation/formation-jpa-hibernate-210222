package fr.dawan.formationjpa.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.heritage.CompteBancaire;
import fr.dawan.formationjpa.entities.heritage.CompteEpargne;
import fr.dawan.formationjpa.entities.heritage.CompteRenumere;

public class HeritageJPA2 {

    public static void main(String[] args) {
       EntityManagerFactory emf= Persistence.createEntityManagerFactory("formationjpa");
       EntityManager em=emf.createEntityManager();
       EntityTransaction tx= em.getTransaction();
       
       CompteBancaire cb1=new CompteBancaire("John Doe","fr32556-3541",500.0);
       CompteEpargne ce1=new CompteEpargne(0.5, "John Doe","fr32556-3542",100.0);
       CompteEpargne ce2=new CompteEpargne(0.5, "Jane Doe","fr32556-35423",50.0);
       CompteRenumere cr1=new CompteRenumere( "Jane Doe","fr32556-354233",50.0);
       tx.begin();
       try {
           em.persist(cb1);
           em.persist(ce1);
           em.persist(ce2);
           em.persist(cr1);
           tx.commit();
    } catch (Exception e) {
        tx.rollback();
        e.printStackTrace();
    }
       em.close();
       emf.close();
    }

}
