package fr.dawan.formationjpa.main;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import fr.dawan.formationjpa.entities.many.Article;
import fr.dawan.formationjpa.entities.many.Marque;

public class ExempleSQLNatif11 {

    public static void main(String[] args) {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        
        Query qr1= em.createNativeQuery("SELECT a.id,a.version,a.description,a.prix,a.date_ajout FROM articles a WHERE a.prix<:prix LIMIT 2");
        List<Object[]> lst=qr1.setParameter("prix",100.0).getResultList();
        for(Object[] tabObj : lst) {
            for(Object result : tabObj) {
                System.out.println(result);
            }
        }
//        qr1.setParameter("prix", 100.0);
//        List<Article> lstAr=(List<Article>)qr1.getResultList();
//        for(Article a: lstAr) {
//            System.out.println(a.getDescription());
//        }
        Query qr2=em.createNamedQuery("Marque.getAllMarque");
        List<Marque> lstMr=(List<Marque>)qr2.getResultList();
        lstMr.forEach(System.out::println);
        em.close();
        emf.close();

    }

}
