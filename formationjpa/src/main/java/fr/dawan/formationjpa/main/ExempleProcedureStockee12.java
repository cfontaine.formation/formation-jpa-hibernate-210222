package fr.dawan.formationjpa.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;

public class ExempleProcedureStockee12 {

    public static void main(String[] args) {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        StoredProcedureQuery  spr=em.createStoredProcedureQuery("GET_COUNT_BY_prix");
        spr.registerStoredProcedureParameter("montant", Double.class, ParameterMode.IN);
        spr.registerStoredProcedureParameter("nbarticles",Integer.class,ParameterMode.OUT);
        
        spr.setParameter("montant",55.0);
        boolean ok=spr.execute();
        System.out.println(ok);
        int result=(Integer)spr.getOutputParameterValue("nbarticles");
        System.out.println(result);
        
        em.close();
        emf.close();
    }

}
