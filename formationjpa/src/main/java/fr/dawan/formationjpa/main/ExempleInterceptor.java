package fr.dawan.formationjpa.main;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.many.Article;

public class ExempleInterceptor {

    public static void main(String[] args) {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        EntityTransaction tx=em.getTransaction();
        
        try {
            tx.begin();
            em.find(Article.class, 1L);

            Article a = new Article("Stylo bille", 2.0, LocalDate.of(2020, 3, 12));
            em.persist(a);

            a.setDateAjout(LocalDate.of(2021, 3, 12));

            //em.remove(a);
            
//            Article a2=em.find(Article.class, 1L);
//            a2.setPrix(4.0);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        em.close();
        emf.close();

    }

}
