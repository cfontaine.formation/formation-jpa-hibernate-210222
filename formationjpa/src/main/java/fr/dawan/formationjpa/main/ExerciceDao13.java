package fr.dawan.formationjpa.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.dao.AuteurDao;
import fr.dawan.formationjpa.dao.CategorieDao;
import fr.dawan.formationjpa.dao.LivreDao;
import fr.dawan.formationjpa.dao.NationDao;
import fr.dawan.formationjpa.entities.bibliotheque.Auteur;
import fr.dawan.formationjpa.entities.bibliotheque.Categorie;
import fr.dawan.formationjpa.entities.bibliotheque.Nation;

public class ExerciceDao13 {

    public static void main(String[] args) {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        
        LivreDao livreDao=new LivreDao(em);
        AuteurDao auteurDao=new AuteurDao(em);
        CategorieDao categorieDao=new CategorieDao(em);
        NationDao nationDao=new NationDao(em);
        
        System.out.println("Nombre de livre=" + livreDao.count());
        
        System.out.println("Livre de 1962");
        livreDao.findByAnnee(1962).forEach(System.out::println);
        
        
        System.out.println("Livres de James Elroy");
        Auteur je=auteurDao.findById(1);
        livreDao.findByAuteur(je).forEach(System.out::println);
        
        System.out.println("Livres de Science Fiction");
        Categorie cat=categorieDao.findById(6);
        livreDao.findByCategorie(cat).forEach(System.out::println);
        
        System.out.println("nombre de livre entre 1970 et 1980");
        System.out.println(livreDao.countByInter(1970, 1980));
        
        System.out.println("Les livres qui ont plusieurs auteurs");
        livreDao.findMultiAuteur().forEach(System.out::println);
        
        System.out.println("Le nombre de livre par auteur");
        livreDao.getStatLivreByAuteur().forEach(System.out::println);
        
        System.out.println("Le nombre de livre par catégorie");
        livreDao.getStatLivreByCategorie().forEach(System.out::println);

        System.out.println("Année=" + livreDao.getMaxLivreAnnee());
        
        auteurDao.findAlive().forEach(System.out::println);
        
        auteurDao.findByNoBook().forEach(System.out::println);
        
        Nation n=nationDao.findById(3);
        
        auteurDao.findByNation(n).forEach(System.out::println);
        
        auteurDao.getTop5AuteurJPQL().forEach(System.out::println);
        
        auteurDao.getTop5Auteur().forEach(System.out::println);
        em.close();
        emf.close();
    }

}
