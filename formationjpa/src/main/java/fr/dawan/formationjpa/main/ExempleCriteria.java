package fr.dawan.formationjpa.main;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fr.dawan.formationjpa.entities.many.Article;

public class ExempleCriteria {

    public static void main(String[] args) {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
       
        CriteriaBuilder cBuilder=em.getCriteriaBuilder();
        CriteriaQuery<Article> cQuery=cBuilder.createQuery(Article.class);
        // SELECT a FROM ARTICLE a WHERE a.prix<100.0 ORDER BY a.prix
        Root<Article> ar=cQuery.from(Article.class);
        cQuery.where(cBuilder.lessThan(ar.get("prix"), 100.0))
        .select(cQuery.getSelection())
        .orderBy(cBuilder.asc(ar.get("prix")));
        
        TypedQuery<Article> query=em.createQuery(cQuery);
        List<Article> lstArticles=query.getResultList();
        lstArticles.forEach(System.out::println);
        
        em.close();
        emf.close();
    }

}
