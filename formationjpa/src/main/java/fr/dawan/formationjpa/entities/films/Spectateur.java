package fr.dawan.formationjpa.entities.films;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="spectateurs")
public class Spectateur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(nullable = false)
    private String prenom;
    
    @Column(nullable = false)
    private String nom;
    
    @OneToMany(mappedBy = "spectateur")
    private List<FilmSpectateur> notes=new ArrayList<>();

    public Spectateur() {
    }

    public Spectateur(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<FilmSpectateur> getNotes() {
        return notes;
    }

    public void setNotes(List<FilmSpectateur> notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "Spectateur [id=" + id + ", prenom=" + prenom + ", nom=" + nom + "]";
    }
    
}
