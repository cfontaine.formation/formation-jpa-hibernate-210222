package fr.dawan.formationjpa.entities.films;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "films")
public class Film implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(nullable = false)
    private String titre;

    @Column(nullable = false)
    private LocalDate dateSortie;

    @Column(length = 1000)
    private String resume;

    @Lob
    private byte[] affiche;

    @OneToMany(mappedBy = "film")
    private List<FilmSpectateur> notes = new ArrayList<>();

    public Film() {
    }

    public Film(String titre, LocalDate dateSortie) {
        this.titre = titre;
        this.dateSortie = dateSortie;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public byte[] getAffiche() {
        return affiche;
    }

    public void setAffiche(byte[] affiche) {
        this.affiche = affiche;
    }

    
    public LocalDate getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(LocalDate dateSortie) {
        this.dateSortie = dateSortie;
    }

    public List<FilmSpectateur> getNotes() {
        return notes;
    }

    public void setNotes(List<FilmSpectateur> notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "Film [id=" + id + ", titre=" + titre + ", dateSortie=" + dateSortie + "]";
    }

    public static byte[] loadImage(String path) throws IOException {
        BufferedImage bi=ImageIO.read(Paths.get(path).toFile());
        ByteArrayOutputStream bo=new ByteArrayOutputStream();
        ImageIO.write(bi,"jpg",bo);
        return bo.toByteArray();
    }
}
