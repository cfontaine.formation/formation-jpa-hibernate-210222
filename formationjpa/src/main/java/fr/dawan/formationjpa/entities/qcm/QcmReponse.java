package fr.dawan.formationjpa.entities.qcm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="reponses")
public class QcmReponse implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;

    @Column(name="resp_text",nullable=false)
    private String respText;
    
    @Column(nullable=false)
    private boolean correct;
    
    @ManyToOne
    private QcmQuestion question;

    public QcmReponse() {
    }

    public QcmReponse(String respText, boolean correct) {
        this.respText = respText;
        this.correct = correct;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRespText() {
        return respText;
    }

    public void setRespText(String respText) {
        this.respText = respText;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public QcmQuestion getQuestion() {
        return question;
    }

    public void setQuestion(QcmQuestion question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "QcmReponse [id=" + id + ", respText=" + respText + ", correct=" + correct + "]";
    }

}

