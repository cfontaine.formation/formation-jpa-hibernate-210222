package fr.dawan.formationjpa.entities.many;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import fr.dawan.formationjpa.entities.AbstractEntity;
import fr.dawan.formationjpa.listeners.CustomListener;
@Entity
@Table(name = "articles")

@NamedQueries({
@NamedQuery(name = "Article.articleInfParam",
query="select a FROM Article a WHERE a.prix < :prix"),

@NamedQuery(name = "Article.articleInterParam",
query="select a FROM Article a WHERE a.prix BETWEEN :prixMin AND :prixMax")
})
@EntityListeners(CustomListener.class)
public class Article extends AbstractEntity {
    private static final long serialVersionUID = 1L;
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long id;
//
//    @Version
//    private int version;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private double prix;

    @Column(name = "date_ajout", nullable = false)
    private LocalDate dateAjout;

    @ManyToOne//(fetch= FetchType.LAZY) // par défaut fetch EAGER
    @JoinColumn(name = "marque_fk")
    private Marque marque;
    
    private LocalDateTime datetimeCreation;
    
    private LocalDateTime dateTimeUpdate;

  //  @ManyToMany
//    @JoinTable(name = "article2fournisseur", 
//    joinColumns = @JoinColumn(name = "aaaaa", referencedColumnName = "id"),
//    inverseJoinColumns = @JoinColumn(name = "bbbb", referencedColumnName = "id"))
 //   private List<Fournisseur> fournisseurs = new ArrayList<>();

    public Article() {
    }

    public Article(String description, double prix, LocalDate dateAjout) {
        this.description = description;
        this.prix = prix;
        this.dateAjout = dateAjout;
    }

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(LocalDate dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

//    public List<Fournisseur> getFournisseurs() {
//        return fournisseurs;
//    }
//
//    public void setFournisseurs(List<Fournisseur> fournisseurs) {
//        this.fournisseurs = fournisseurs;
//    }

    @Override
    public String toString() {
        return "Article [id=" + super.getId()+ ", description=" + description + ", prix=" + prix
                + ", dateAjout=" + dateAjout + "]";
    }
    
//    @PostLoad
//    void onPostLoad() {
//        System.out.println("post load");
//}
    

}
