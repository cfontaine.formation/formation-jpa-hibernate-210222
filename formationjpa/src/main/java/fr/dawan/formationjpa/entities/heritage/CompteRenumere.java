package fr.dawan.formationjpa.entities.heritage;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
//@DiscriminatorValue("CR")
public class CompteRenumere extends CompteBancaire {

    private static final long serialVersionUID = 1L;
    
    private double valOperation=0.01;

    public CompteRenumere() {
        super();
    }

    public CompteRenumere(String titulaire, String iban, double solde) {
        super(titulaire, iban, solde);
    }

    public double getValOperation() {
        return valOperation;
    }

    public void setValOperation(double valOperation) {
        this.valOperation = valOperation;
    }
    
    
    

    @Override
    public void crediter(double valeur) {
        super.crediter(valeur);
        solde+=valOperation;
    }

    @Override
    public void debiter(double valeur) {
        super.debiter(valeur);
        solde+=valOperation;
    }

    @Override
    public String toString() {
        return "CompteRenumere [valOperation=" + valOperation + ", toString()=" + super.toString() + "]";
    }

}
