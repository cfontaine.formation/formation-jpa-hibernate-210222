package fr.dawan.formationjpa.entities.films;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table (name="films_spectateurs")
public class FilmSpectateur implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    private FilmSpectateurPK id=new FilmSpectateurPK();
    
    @ManyToOne
    @MapsId("filmId")
    @JoinColumn(name="film_id")
    private Film film;
    
    @ManyToOne
    @MapsId("spectateurId")
    @JoinColumn(name="spectateur_id")
    private Spectateur spectateur;
    
    private int note;
    
    public FilmSpectateur() {
    }
    
    public FilmSpectateur(Film film, Spectateur spectateur, int note) {
        this.film = film;
        this.spectateur = spectateur;
        this.note = note;
    }

    public FilmSpectateurPK getId() {
        return id;
    }

    public void setId(FilmSpectateurPK id) {
        this.id = id;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Spectateur getSpectateur() {
        return spectateur;
    }

    public void setSpectateur(Spectateur spectateur) {
        this.spectateur = spectateur;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }
}
