package fr.dawan.formationjpa.entities.many;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import fr.dawan.formationjpa.entities.AbstractEntity;

@Entity
@Table(name = "Fournisseurs")
public class Fournisseur extends AbstractEntity {

    private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long id;
//
//    @Version
//    private int version;

    @Column(length = 50, nullable = false)
    private String nom;

    @ManyToMany//(fetch=FetchType.EAGER)/*(mappedBy = "fournisseurs")*/
    private List<Article> articles = new ArrayList<>();

    public Fournisseur() {
    }

    public Fournisseur(String nom) {
        this.nom = nom;
    }

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "Fournisseur [id=" + super.getId() + ", nom=" + nom + "]";
    }
}
