package fr.dawan.formationjpa.entities.bibliotheque;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fr.dawan.formationjpa.entities.AbstractEntity;

@Entity
@Table(name = "nations")
public class Nation extends AbstractEntity { // implements Serializable

    private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long id;
//    
//    @Version
//    private int version;

    @Column(length = 100, nullable = false)
    private String nom;

    @OneToMany(mappedBy = "nationalite")
    private List<Auteur> auteurs = new ArrayList<>();

    public Nation() {
    }

    public Nation(String nom) {
        this.nom = nom;
    }

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Auteur> getAuteurs() {
        return auteurs;
    }

    public void setAuteurs(List<Auteur> auteurs) {
        this.auteurs = auteurs;
    }

    @Override
    public String toString() {
        return "Nation [id=" + super.getId() + ", nom=" + nom + "]";
    }
}
