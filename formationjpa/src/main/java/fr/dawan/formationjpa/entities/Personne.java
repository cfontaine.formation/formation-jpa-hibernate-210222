package fr.dawan.formationjpa.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import fr.dawan.formationjpa.enums.Civilite;

@Entity
@Table(name = "personnes")
//@TableGenerator(name = "personnegenerator")
//@SequenceGenerator(name="personneseq")
public class Personne  extends AbstractDbObject/*implements Serializable*/ {

    private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personneseq")
//    private long id;

    @Column(length = 60)
    private String prenom;

//    @Column(length = 60, columnDefinition = " VARCHAR(60) DEFAULT 'Doe'")
    @Column(length = 60, nullable = false)
    private String nom;

    @Column(length = 1024, nullable = false) // unique=true
    private String email;

    @Enumerated(EnumType.STRING)
    private Civilite civilite;

//    @Temporal(TemporalType.DATE)
//    private Date dateNaissance;
    @Column(name = "date_naissance", nullable = false)
    private LocalDate dateNaissance;

    @Embedded
    private Adresse adressePerso;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "codePostal", column = @Column(name = "code_postal_pro")),
            @AttributeOverride(name = "ville", column = @Column(name = "ville_pro")),
            @AttributeOverride(name = "rue", column = @Column(name = "rue_pro")) })

    private Adresse adressePro;

    @Transient
    private int nePaspersiter;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name="numero_telephone",
    joinColumns = @JoinColumn(name="personne_id"))
    private List<String> numeroTelephone=new ArrayList<>();
    
    public Personne() {
    }

    public Personne(String prenom, String nom, String email) {
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
    }

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNePaspersiter() {
        return nePaspersiter;
    }

    public void setNePaspersiter(int nePaspersiter) {
        this.nePaspersiter = nePaspersiter;
    }

    public Civilite getCivilite() {
        return civilite;
    }

    public void setCivilite(Civilite civilite) {
        this.civilite = civilite;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Adresse getAdressePerso() {
        return adressePerso;
    }

    public void setAdressePerso(Adresse adressePerso) {
        this.adressePerso = adressePerso;
    }

    public Adresse getAdressePro() {
        return adressePro;
    }

    public void setAdressePro(Adresse adressePro) {
        this.adressePro = adressePro;
    }

    @Override
    public String toString() {
        return "Personne [prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", civilite=" + civilite
                + ", dateNaissance=" + dateNaissance + ", adressePerso=" + adressePerso + ", adressePro=" + adressePro
                + ", nePaspersiter=" + nePaspersiter + ", toString()=" + super.toString() + "]";
    }

//    @Override
//    public String toString() {
//        return "Personne [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", email=" + email + "]";
//    }

}
