package fr.dawan.formationjpa.entities.monument;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import fr.dawan.formationjpa.entities.AbstractEntity;

@Entity
@Table(name = "monuments")
public class Monument extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    
    @Column(nullable = false)
    private String nom;
    
    @Column(nullable = false)
    private int anneeConstruction;
    
    @OneToOne
    private Coordonnee coordonee;
    
    @ManyToOne
    private Nation nation;
    
    @ManyToMany
    private Set<Etiquette> etiquettes=new HashSet<>();

    public Monument() {
    }

    public Monument(String nom, int anneeConstruction, Coordonnee coordonee, Nation nation) {
        this.nom = nom;
        this.anneeConstruction = anneeConstruction;
        this.coordonee = coordonee;
        this.nation = nation;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAnneeConstruction() {
        return anneeConstruction;
    }

    public void setAnneeConstruction(int anneeConstruction) {
        this.anneeConstruction = anneeConstruction;
    }

    
    
    public Coordonnee getCoordonee() {
        return coordonee;
    }

    public void setCoordonee(Coordonnee coordonee) {
        this.coordonee = coordonee;
    }

    public Nation getNation() {
        return nation;
    }

    public void setNation(Nation nation) {
        this.nation = nation;
    }

    public Set<Etiquette> getEtiquettes() {
        return etiquettes;
    }

    public void setEtiquettes(Set<Etiquette> etiquettes) {
        this.etiquettes = etiquettes;
    }

    @Override
    public String toString() {
        return "Monument [nom=" + nom + ", anneeConstruction=" + anneeConstruction + "]";
    }

}
