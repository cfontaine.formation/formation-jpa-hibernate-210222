package fr.dawan.formationjpa.entities.qcm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
@Entity
@Table(name="questions")
public class QcmQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;

    @Column(name="qst_text",nullable = false)
    private String qstText;

    @Column(nullable = false)
    private boolean multiple;

    @Column(name="num_order")
    private int numOrder;
    
    @ManyToOne
    private Qcm qcm;

    public QcmQuestion() {
    }

    public QcmQuestion(String qstText, boolean multiple, int numOrder) {
        this.qstText = qstText;
        this.multiple = multiple;
        this.numOrder = numOrder;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQstText() {
        return qstText;
    }

    public void setQstText(String qstText) {
        this.qstText = qstText;
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public int getNumOrder() {
        return numOrder;
    }

    public void setNumOrder(int numOrder) {
        this.numOrder = numOrder;
    }
    
    public Qcm getQcm() {
        return qcm;
    }

    public void setQcm(Qcm qcm) {
        this.qcm = qcm;
    }

    @Override
    public String toString() {
        return "QcmQuestion [id=" + id + ", qstText=" + qstText + ", multiple=" + multiple + ", numOrder=" + numOrder
                + "]";
    }

}
