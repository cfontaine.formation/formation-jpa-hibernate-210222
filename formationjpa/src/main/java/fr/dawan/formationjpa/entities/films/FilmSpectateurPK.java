package fr.dawan.formationjpa.entities.films;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FilmSpectateurPK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "film_id")
    private long filmId;

    @Column(name = "spectateur_id")
    private long spectateurId;

    public FilmSpectateurPK() {
    }

    public FilmSpectateurPK(long filmId, long spectateurId) {
        this.filmId = filmId;
        this.spectateurId = spectateurId;
    }

    public long getFilmId() {
        return filmId;
    }

    public void setFilmId(long filmId) {
        this.filmId = filmId;
    }

    public long getSpectateurId() {
        return spectateurId;
    }

    public void setSpectateurId(long spectateurId) {
        this.spectateurId = spectateurId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(filmId, spectateurId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FilmSpectateurPK other = (FilmSpectateurPK) obj;
        return filmId == other.filmId && spectateurId == other.spectateurId;
    }

}
