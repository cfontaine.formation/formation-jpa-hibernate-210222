package fr.dawan.formationjpa.entities.bibliotheque;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fr.dawan.formationjpa.entities.AbstractEntity;

@Entity
@Table(name = "categories")
public class Categorie extends AbstractEntity { //implements Serializable

    private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long id;
//    
//    @Version
//    private int version;

    @Column(nullable = false)
    private String nom;
    
    @OneToMany(mappedBy = "categorie")
    private List<Livre> livres=new ArrayList<>();

    public Categorie() {
    }

    public Categorie(String nom) {
        this.nom = nom;
    }

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Livre> getLivres() {
        return livres;
    }

    public void setLivres(List<Livre> livres) {
        this.livres = livres;
    }

    @Override
    public String toString() {
        return "Categorie [id=" + super.getId() + ", nom=" + nom + "]";
    }
}
