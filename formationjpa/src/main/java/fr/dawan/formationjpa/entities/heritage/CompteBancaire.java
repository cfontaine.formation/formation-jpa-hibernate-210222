package fr.dawan.formationjpa.entities.heritage;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "compte_bancaire")

//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@DiscriminatorColumn(name="typeCompte")

// @Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

@Inheritance(strategy = InheritanceType.JOINED)
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue/*(strategy = GenerationType.IDENTITY)*/
    private long id;

    private String titulaire;

    private String iban;

    protected double solde;

    public CompteBancaire() {
    }

    public CompteBancaire(String titulaire, String iban, double solde) {
        this.titulaire = titulaire;
        this.iban = iban;
        this.solde = solde;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public void crediter(double valeur) {
        if(valeur>=0.0) {
            solde+=valeur;
        }
    }
    
    public void debiter(double valeur) {
        if(valeur>=0.0) {
            solde-=valeur;
        }  
    }

    @Override
    public String toString() {
        return "CompteBancaire [id=" + id + ", titulaire=" + titulaire + ", iban=" + iban + ", solde=" + solde + "]";
    }

}
