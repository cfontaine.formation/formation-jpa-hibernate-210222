package fr.dawan.formationjpa.entities.many;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fr.dawan.formationjpa.entities.AbstractEntity;

@Entity
@Table(name = "marques")

@NamedNativeQuery( name= "Marque.getAllMarque",
    query="SELECT m.id, m.version, m.nom FROM marques m ",
    resultClass = Marque.class
        )
public class Marque extends AbstractEntity { //implements Serializable

    private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long id;
//
//    @Version
//    private int version;

    @Column(length = 50, nullable = false)
    private String nom;

    @OneToMany(mappedBy = "marque",cascade = CascadeType.ALL)
    private List<Article> articles=new ArrayList<>();

    public Marque() {
    }

    public Marque(String nom) {
        this.nom = nom;
    }

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "Marque [id=" + super.getId() + ", nom=" + nom + "]";
    }

}
