package fr.dawan.formationjpa.entities.bibliotheque;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import fr.dawan.formationjpa.entities.AbstractEntity;

@Entity
@Table(name = "auteurs")

@NamedQueries({
    @NamedQuery(
        name="Auteur.auteurAlive",
        query = "SELECT a FROM Auteur a WHERE a.deces IS NULL"
    ),
    @NamedQuery(
        name="Auteur.auteurNoBook",
        query = "SELECT a FROM Auteur a LEFT JOIN a.livres l WHERE l IS NULL"
    ),
    @NamedQuery(
        name="Auteur.auteurByNation",
        query = "SELECT a FROM Auteur a WHERE a.nationalite=:nation"
    )
})
public class Auteur extends AbstractEntity { // implements Serializable

    private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long id;
//    
//    @Version
//    private int version; 

    @Column(length = 50, nullable = false)
    private String prenom;

    @Column(length = 50, nullable = false)
    private String nom;

    @Column(nullable = false)
    private LocalDate naissance;

    private LocalDate deces;
    
    @ManyToOne
    private Nation nationalite;
    
    @ManyToMany(mappedBy="auteurs")//, fetch = FetchType.EAGER)
    private List<Livre> livres=new ArrayList<>();

    public Auteur() {
    }

    public Auteur(String prenom, String nom, LocalDate naissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.naissance = naissance;
    }

    public Auteur(String prenom, String nom, LocalDate naissance, LocalDate deces) {
        this.prenom = prenom;
        this.nom = nom;
        this.naissance = naissance;
        this.deces = deces;
    }

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getNaissance() {
        return naissance;
    }

    public void setNaissance(LocalDate naissance) {
        this.naissance = naissance;
    }

    public LocalDate getDeces() {
        return deces;
    }

    public void setDeces(LocalDate deces) {
        this.deces = deces;
    }

    public Nation getNationalite() {
        return nationalite;
    }

    public void setNationalite(Nation nationalite) {
        this.nationalite = nationalite;
    }
    
    public List<Livre> getLivres() {
        return livres;
    }

    public void setLivres(List<Livre> livres) {
        this.livres = livres;
    }

    @Override
    public String toString() {
        return "Auteur [id=" + super.getId() + ", prenom=" + prenom + ", nom=" + nom + ", naissance=" + naissance + ", deces="
                + deces + "]";
    }

}
