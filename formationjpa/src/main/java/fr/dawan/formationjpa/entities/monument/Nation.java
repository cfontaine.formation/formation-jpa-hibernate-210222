package fr.dawan.formationjpa.entities.monument;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import fr.dawan.formationjpa.entities.AbstractEntity;

@Entity(name = "nationMonument")
@Table(name = "nations_monument")
public class Nation extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(length = 100,nullable = false)
    private String nom;

    public Nation() {
    }

    public Nation(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Nation [nom=" + nom + "]";
    }
}
