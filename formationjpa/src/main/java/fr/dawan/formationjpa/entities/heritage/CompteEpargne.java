package fr.dawan.formationjpa.entities.heritage;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
// @DiscriminatorValue("Ce")
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;

    private double taux=0.05;

    public CompteEpargne() {

    }
    
    public CompteEpargne(double taux) {
        this.taux = taux;
    }



    public CompteEpargne(double taux, String titulaire, String iban, double solde) {
        super(titulaire, iban, solde);
    }


    public void calculSolde() {
        solde*=(1+taux*100);
    }

    public double getTaux() {
        return taux;
    }



    public void setTaux(double taux) {
        this.taux = taux;
    }

    @Override
    public String toString() {
        return "CompteEpagne [taux=" + taux + ", toString()=" + super.toString() + "]";
    }
    
    
    
}
