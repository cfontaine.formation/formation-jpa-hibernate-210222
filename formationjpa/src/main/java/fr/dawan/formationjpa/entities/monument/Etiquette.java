package fr.dawan.formationjpa.entities.monument;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import fr.dawan.formationjpa.entities.AbstractEntity;

@Entity
@Table(name = "etiquettes")
public class Etiquette extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private String nom;

    @ManyToMany(mappedBy = "etiquettes")
    private Set<Monument> monuments = new HashSet<>();

    public Etiquette() {
    }

    public Etiquette(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Monument> getMonuments() {
        return monuments;
    }

    public void setMonuments(Set<Monument> monuments) {
        this.monuments = monuments;
    }

    @Override
    public String toString() {
        return "Etiquette [nom=" + nom + "]";
    }
}
