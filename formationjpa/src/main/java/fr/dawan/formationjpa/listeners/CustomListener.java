package fr.dawan.formationjpa.listeners;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import fr.dawan.formationjpa.entities.many.Article;

public class CustomListener {

//  }
//  @PostUpdate
//  void onPostUpdate() {
//      System.out.println("post update");
//  }
  @PreUpdate
  void onPreUpdate(Article  a) {
      System.out.println("pre update" + a);
      //adateTimeUpdate=LocalDateTime.now();
  }
//  @PostPersist
//  void onPostPersist() {
//      System.out.println("post persist");
//  }
  @PrePersist
  void onPrePersist(Article  a) {
      System.out.println("pre persist" + a);
//      datetimeCreation=LocalDateTime.now();
//      dateTimeUpdate=LocalDateTime.now();
  }
}
