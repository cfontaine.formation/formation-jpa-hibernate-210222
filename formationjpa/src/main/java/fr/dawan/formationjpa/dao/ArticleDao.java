package fr.dawan.formationjpa.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import fr.dawan.formationjpa.entities.many.Article;

public class ArticleDao extends AbstractGenericDao<Article> {

    public ArticleDao(EntityManager em) {
        super(em, Article.class);
    }
    
    public List<Article> findByPrixInf(double prix){
        TypedQuery<Article> query=em.createQuery("SELECT a FROM Article a WHERE a.prix< :prix",Article.class);
        query.setParameter("prix", prix);
        return query.getResultList();
    }
    

}