package fr.dawan.formationjpa.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import fr.dawan.formationjpa.entities.bibliotheque.Auteur;
import fr.dawan.formationjpa.entities.bibliotheque.Categorie;
import fr.dawan.formationjpa.entities.bibliotheque.Livre;

public class LivreDao extends AbstractGenericDao<Livre> {

    public LivreDao(EntityManager em) {
        super(em, Livre.class);
    }

    public List<Livre> findByAnnee(int anneeSortie){
        TypedQuery<Livre> query=em.createQuery("SELECT l FROM Livre l WHERE anneeSortie=:annee",Livre.class);
        query.setParameter("annee", anneeSortie);
        return query.getResultList();
    }
    
    public List<Livre> findByCategorie(Categorie categorie){
        TypedQuery<Livre> query=em.createQuery("SELECT l FROM Livre l JOIN l.categorie c WHERE c=:categorie  ORDER BY l.anneeSortie",Livre.class);
        query.setParameter("categorie", categorie);
        return query.getResultList();
    }
    
    public List<Livre> findByAuteur(Auteur auteur){
        TypedQuery<Livre> query=em.createQuery("SELECT l FROM Livre l JOIN l.auteurs a WHERE a=:auteur ORDER BY l.anneeSortie",Livre.class);
        query.setParameter("auteur", auteur);
        return query.getResultList();
    }
    
    public long countByInter(int anneeMin, int anneeMax) {
        TypedQuery<Long> query=em.createQuery("SELECT COUNT(l) FROM Livre l WHERE l.anneeSortie BETWEEN :min AND :max",Long.class);
        return query.setParameter("min", anneeMin)
                    .setParameter("max",anneeMax)
                    .getSingleResult();
    }
    
    public List<Livre> findMultiAuteur(){
        TypedQuery<Livre> query=em.createQuery("SELECT l FROM Livre l JOIN l.auteurs a GROUP BY l.id HAVING COUNT(a)>1 ORDER BY l.anneeSortie",Livre.class);
        return query.getResultList();
    }
    
    public List<StatLivre> getStatLivreByAuteur(){
        TypedQuery<StatLivre> query=em.createQuery("SELECT new fr.dawan.formationjpa.dao.StatLivre(COUNT(l),CONCAT(a.prenom,' ',a.nom)) FROM Livre l Join l.auteurs a GROUP BY a.id",StatLivre.class);
        return query.getResultList();
    }
    
    public List<StatLivre> getStatLivreByCategorie(){
        TypedQuery<StatLivre> query=em.createQuery("SELECT new fr.dawan.formationjpa.dao.StatLivre(COUNT(l),l.categorie.nom) FROM Livre l Join l.categorie c GROUP BY c.id",StatLivre.class);
        return query.getResultList();
    }
    
    public int getMaxLivreAnnee(){
        Query query=em.createNativeQuery("SELECT annee_sortie FROM livres GROUP BY annee_sortie ORDER BY COUNT(livres.id) DESC LIMIT 1 ");
        return (Integer) query.getSingleResult();
    }
}
