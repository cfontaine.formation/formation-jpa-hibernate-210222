package fr.dawan.formationjpa.dao;

import javax.persistence.EntityManager;

import fr.dawan.formationjpa.entities.bibliotheque.Nation;

public class NationDao extends AbstractGenericDao<Nation> {

    public NationDao(EntityManager em) {
        super(em, Nation.class);
    }

}
