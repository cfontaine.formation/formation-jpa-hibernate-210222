package fr.dawan.formationjpa.dao;

import javax.persistence.EntityManager;

import fr.dawan.formationjpa.entities.bibliotheque.Categorie;

public class CategorieDao extends AbstractGenericDao<Categorie> {

    public CategorieDao(EntityManager em) {
        super(em, Categorie.class);
    }

}
