package fr.dawan.formationjpa.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import fr.dawan.formationjpa.entities.AbstractEntity;

public abstract class AbstractGenericDao<T extends AbstractEntity> {

    protected EntityManager em;

    private Class<T> classEntity;

    protected AbstractGenericDao(EntityManager em, Class<T> classEntity) {
        this.em = em;
        this.classEntity = classEntity;
    }

    public void saveOrUpdate(T elm) throws Exception {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            if (elm.getId() == 0) {
                em.persist(elm);
            } else {
                em.merge(elm);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }
    
    public void remove(T elm) throws Exception {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.remove(elm);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }
    
    public void remove(long id) throws Exception {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            T elm=em.find(classEntity,id );
            if(elm!=null) {
                em.remove(elm);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }
    
    public T findById(long id) {
        return em.find(classEntity,id );
    }
    
    public List<T> findAll(){
        TypedQuery<T> query= em.createQuery("SELECT e FROM "+ classEntity.getName() + " e", classEntity);
        return query.getResultList();
    }
    
    public long count() {
        TypedQuery<Long> query=em.createQuery("SELECT COUNT(e) FROM "+ classEntity.getName() + " e",Long.class);
        return query.getSingleResult();
    }
    
    public List<T> findPartial(int start, int nbElement){
        TypedQuery<T> query= em.createQuery("SELECT e FROM "+ classEntity.getName() + " e", classEntity);
        return query.setFirstResult(start).setMaxResults(nbElement).getResultList();
    }
    
    public EntityManager getEntityManger() {
        return em;
    }

    public void setEntityManger(EntityManager em) {
        this.em = em;
    }
}
