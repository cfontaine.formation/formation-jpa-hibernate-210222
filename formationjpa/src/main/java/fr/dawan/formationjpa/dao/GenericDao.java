package fr.dawan.formationjpa.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import fr.dawan.formationjpa.entities.AbstractEntity;

public class GenericDao {

    public static <T extends AbstractEntity> void saveOrUpdate(T elm,EntityManager em,boolean close) throws Exception {
        EntityTransaction tx=em.getTransaction();
        try {
            tx.begin();
            if(elm.getId()==0) {
                em.persist(elm);
            }
            else {
                em.merge(elm);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        } finally {
            if(close) {
                em.close();
            }
        } 
    }
    
    public static <T extends AbstractEntity> void remove(T elm,EntityManager em, boolean close) throws Exception {
        EntityTransaction tx=em.getTransaction();
        try {
            tx.begin();
            if(elm.getId()!=0) {
                em.remove(elm);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        } finally {
            if(close) {
                em.close();
            }
        } 
    }

    public static <T extends AbstractEntity> T findById(Class<T> clazz,long id,EntityManager em, boolean close) {
        T elm=em.find(clazz, id);
        if(close) {
            em.close();
        }
        return elm;
    }
    
    public static <T extends AbstractEntity> List<T> findAll(Class<T> clazz,EntityManager em, boolean close){
        TypedQuery<T> query=em.createQuery("select e from "+ clazz.getName() + " e", clazz);
        List<T> tmp=query.getResultList();
        if(close) {
            em.close();
        }
        return tmp;
    }
}
