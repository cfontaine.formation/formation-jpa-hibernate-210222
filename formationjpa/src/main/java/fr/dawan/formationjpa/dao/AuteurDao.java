package fr.dawan.formationjpa.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import fr.dawan.formationjpa.entities.bibliotheque.Auteur;
import fr.dawan.formationjpa.entities.bibliotheque.Nation;

public class AuteurDao extends AbstractGenericDao<Auteur> {

    public AuteurDao(EntityManager em) {
        super(em, Auteur.class);
    }
    
    public List<Auteur> findAlive(){
        TypedQuery<Auteur> query=em.createNamedQuery("Auteur.auteurAlive", Auteur.class);
        return query.getResultList();
    }
    
    public List<Auteur> findByNoBook(){
        TypedQuery<Auteur> query=em.createNamedQuery("Auteur.auteurNoBook", Auteur.class);
        return query.getResultList();
    }
    
    public List<Auteur> findByNation(Nation nation){
        TypedQuery<Auteur> query=em.createNamedQuery("Auteur.auteurByNation",Auteur.class);
        return query.setParameter("nation",nation).getResultList();
    }
    
    public List<Auteur> getTop5Auteur(){
        Query query=em.createNativeQuery("SELECT auteurs.id, auteurs.version, auteurs.prenom, auteurs.nom, auteurs.naissance, auteurs.deces, auteurs.nationalite_id FROM Livres INNER JOIN livres_auteurs ON livres.id=livres_auteurs.livres_id  INNER  JOIN auteurs ON livres_auteurs.auteurs_id = auteurs.id GROUP BY auteurs.id ORDER BY COUNT(livres.id) DESC LIMIT 5",Auteur.class);
        return (List<Auteur>)query.getResultList();
     } 
    
    public List<Auteur> getTop5AuteurJPQL(){
       TypedQuery<Auteur> query=em.createQuery("SELECT a FROM Auteur a JOIN a.livres l GROUP BY a ORDER BY COUNT(l) DESC",Auteur.class);
       return query.setMaxResults(5).getResultList();
    }
}

