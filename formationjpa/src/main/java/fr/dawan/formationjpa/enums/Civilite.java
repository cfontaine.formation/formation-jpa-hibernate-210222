package fr.dawan.formationjpa.enums;

public enum Civilite {
    MADAME,MONSIEUR,AUTRE
}
